package controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @RequestMapping("/hello")
    public String greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return "hello worldx! " + name;
    }

    @RequestMapping("/user")
    public String user(@RequestParam(value="name", defaultValue="wissem") String name){
        return "hello user xx : " + name;
    }
}